#!/bin/sh
# Generate a password
# Usage : sh ./PasswordGenerator.sh [lenght]

lenght=8

if [ "$#" -eq 1 ] ; then
    test $1 -eq 0 2>/dev/null
    if ! [ $? -eq 2 ]; then
        lenght=$1
    fi
fi

apg -a 1 -m ${lenght} -M NCL -n 1 -c /dev/urandom
