#!/bin/sh
# List all folders from a directory and display the size of each
# Usage : sh foldersSize.sh [Directory]

cd ${1}
for inode in `ls`
do
    if
	[ -d $inode ]
    then
	du -sh $inode
    fi
done
