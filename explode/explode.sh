#!/bin/sh
# Like explode with PHP
# Usage : explode "DELIMITEUR" "STRING AVEC LA CONTENU A EXPLODE"
#       for i in "${!a[@]}"; do
#           printf "%d %s\n" "$i"  "${a[$i]}"
#       done

function explode {
    string=$2
    delim=$1
    a=( $(echo "${string//[${delim}]/ }") )
}
