#!/bin/bash
# Like die with PHP
# Usage : die "ERROR MESSAGE"

die () {
    echo >&2 "$@"
    exit 1
}
